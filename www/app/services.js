angular.module('zampieri.services', [])

.constant('zampieri', {
        "url": "http://savo.zampieriimoveis.com.br/zampieri-api/public"
    })
    .factory('requisicao', function($http, zampieri) {

        return function(url, success, error, method, params) {

            $http({
                    'url': zampieri.url + url,
                    'method': method,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    transformRequest: function(obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    'data': params
                })
                .then(
                    function successCallback(response) {
                        if (typeof(success) == "function")
                            success(response);
                    },
                    function errorCallback(response) {
                        if (typeof(error) == "function")
                            error(response);
                    }
                );
        }
    })
    .factory('popup', function($ionicPopup) {
        return function(titulo, msg) {
            $ionicPopup.alert({
                title: titulo,
                template: msg,
                okText: 'Prosseguir'
            });
        }
    })
    .factory('api', function($http, requisicao) {


        return {
            login: function(loginData, success, error) { //login service
                requisicao('/api/auth', success, error, "POST", loginData);
            },
            enviarCadastro: function(formCad, success, error){ //cadastro user service
                requisicao('/api/usuario/new/user', success, error, "POST", formCad);
            },
            listIndicacoes: function(usuario_id, success, error) { //listar indicaoes por usuario service
                requisicao('/api/usuario/list/indicacao', success, error, "POST", usuario_id);
            },
            enviarIndicacaoInquilino: function(indicacao, success, error) { // cadastrar indicacao de inquilino service
                requisicao('/api/usuario/new/indicacao/inquilino', success, error, "POST", indicacao);
            },
            enviarIndicacaoImovel:function(indicacao, success, error){ // cadastrar indicacao de imovel service
                requisicao('/api/usuario/new/indicacao/imovel', success, error,"POST", indicacao);
            },
            enviarSolicitacaoPagamento: function(pagamento, success, error) { // cadastrar solicitacao de pagamento service
                requisicao('/api/usuario/new/pagamento', success, error, "POST", pagamento);
            }, 
            getExtrato: function(usuario_id, success, error){ // listar extrato por id service
                requisicao('/api/usuario/extrato', success, error, "POST", usuario_id);
            },
            getInfos: function(usuario_id, success, error){ // get infos
                requisicao('/api/usuario/infos', success, error, "POST", usuario_id);
            },
            enviarRemember: function(usuario_id, success, error){ // enviar recuperar senha
                requisicao('/api/usuario/recovery', success, error, "POST", usuario_id);
            },
            changePassword: function(passwordData, success, error){ //  change password
                requisicao('/api/usuario/changepassword', success, error, "POST", passwordData);
            },
            enviarMsg: function(data, success, error){ 
                requisicao('/api/usuario/send/msg', success, error, "POST", data);
            },
            getMsgs: function(data, success, error){ 
                requisicao('/api/usuario/get/msg', success, error, "POST", data);
            }
        };
    });