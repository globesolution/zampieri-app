angular.module('zampieri.remember', [])

.controller('rememberCtrl', function($scope, $state, api, popup, $location) {
    $scope.loginData = {};
    $scope.enviarRemember = function() {
        // criar validações
        api.enviarRemember($scope.loginData,
            function(res) {
                if (res.data.status == 200) {
                    console.log(res.data);
                    popup("Sucesso", res.data.msg);
                    $location.path("/login");

                } else {
                    console.log('erro');
                    popup("Opss...", res.data.msg);
                }
            },
            function(res) {
                console.log(res);
            }
        );
    }
});