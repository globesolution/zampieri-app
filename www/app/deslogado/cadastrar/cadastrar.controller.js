angular.module('zampieri.cadastro', [])

.controller('cadastrarCtrl', function($scope, $state, api, popup, $location) {
    $scope.userData = {};
    $scope.enviarCadastro = function() {
        // criar validações
        if($scope.userData.termos){
            api.enviarCadastro($scope.userData,
                function(res) {
                    if (res.data.status == 200) {
                        console.log(res.data);
                        popup("Sucesso", res.data.msg);
                        $location.path("/login");

                    } else {
                        console.log('erro');
                        popup("Opss...", res.data.msg);
                    }
                },
                function(res) {
                    console.log(res);
                }
            );
        }else{
            popup('Opss...', "Você precisa concordar com os termos de uso.");
        }
    }
});