angular.module('zampieri.login', [])


//login controller
.controller('loginCtrl', function($scope, $stateParams, api, $state, $rootScope, popup, $cordovaSQLite) {

    $scope.loginData = {};

    $rootScope.checkDB = function(){
        console.log(db);
        var query = "SELECT email,password FROM user WHERE 1";
        $cordovaSQLite.execute(db, query).then(function(res) {
            if(res.rows.length > 0) {               
                var data = res.rows.item(0);
                console.log(data);
                $scope.loginData.email = data.email;
                $scope.loginData.senha = data.password;
                $scope.doLogin();

            } else {
                console.log("No results found");
                return false;
            }
        }, function (err) {
            console.error(err);
        });

        console.log(db.executeSql(query));

    }
    $scope.doLogin = function() {
        if ($scope.loginData.email == null || $scope.loginData.senha == null) {
            popup("Opsss", "Parece que você esqueceu de algo.");
            return;
        } else {
            api.login($scope.loginData,
                function(res) {
                    if (res.data.status == 200) {
                        $rootScope.dataLogin = res.data;
                        if($scope.loginData.remember){
                            $scope.saveDBLocal($scope.loginData);
                        }
                        $rootScope.changeButtonStatus();
                        if (res.data.msgnovidade.length != 0) {
                            popup("Novidade", res.data.msgnovidade);
                        }
                        $state.go('tab.indicar');
                    } else
                        popup("Opsss", res.data.msg);
                },
                function(res) {
                    console.log(res);
                }
            );
        }
    }
    $scope.doCadastro = function() {
        $state.go('cadastrar'); 
    }
    $scope.doRemember = function(){
        $state.go('remember');
    }
    $scope.saveDBLocal = function(loginData){
        var query = "DELETE FROM user WHERE 1";
        $cordovaSQLite.execute(db, query);
        var query = "INSERT INTO user (email, password) VALUES (?,?)";
        $cordovaSQLite.execute(db, query, [loginData.email, loginData.senha]);


        var query = "SELECT * FROM user WHERE 1";
        a = $cordovaSQLite.execute(db, query);
        console.log(a);
    }

});