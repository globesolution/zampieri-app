angular.module('zampieri.indicacoesrealizadas', [])

.controller('indicacoesRealizadasCtrl', function($scope, $stateParams, api, $rootScope) {
    $scope.indicacoes = [];

    $scope.setDate = function(data){
        resgistro = new Date(data);
        return resgistro;
    } 

    $scope.getIconIndicationType = function(tipo) {
        if (tipo == 1) {
            return "ion-person";
        } else {
            return "ion-home";
        }
    }
    $scope.getIndications = function() {

        $scope.msgNoIndica = false;

        usuario_id = {
            'usuario_id': $rootScope.dataLogin.usuario.id
        };

        api.listIndicacoes(usuario_id,
            function(res) {
                if (res.data.status == 200) {
                    console.log(res.data);
                    if (res.data.indicacoes.length != 0) {
                        $scope.msgNoIndica = false;
                        $scope.indicacoes = res.data.indicacoes;
                    } else {
                        $scope.msgNoIndica = true;
                    }
                } else
                    console.log('erro');
            },
            function(res) {
                console.log(res);
            }
        );

    }
});