angular.module('zampieri.dashboard', [])


// dash controllers
.controller('DashCtrl', function($scope, $rootScope, popup, api) {


    $scope.saldo = 0.00;
    $rootScope.atualizarSaldo = function() {
        $scope.saldo = $rootScope.dataLogin.usuario.saldo;
    }
    $scope.getInfos = function() {
        usuario_id = {
            'usuario_id': $rootScope.dataLogin.usuario.id
        };

        api.getInfos(usuario_id,
            function(res) {
                if (res.data.status == 200) {
                    $scope.infos = res.data.infos
                } else
                    popup("Opss...", res.data.msg);
            },
            function(res) {
                console.log(res);
            }
        );
        $rootScope.atualizarSaldo();
    }

});