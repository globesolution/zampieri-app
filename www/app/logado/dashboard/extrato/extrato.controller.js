angular.module('zampieri.extrato', [])





.controller('extratoCtrl', function($scope, $stateParams, api, $rootScope) {
    $scope.extrato = [];
    $scope.msgNoExtrato = false;

    $scope.setDate = function(data){
        resgistro = new Date(data);
        return resgistro;
    } 

    $scope.getExtrato = function() {
        usuario_id = {
            'usuario_id': $rootScope.dataLogin.usuario.id
        };

        api.getExtrato(usuario_id,
            function(res) {
                if (res.data.status == 200) {
                    console.log(res.data);
                    if (res.data.extrato.length != 0) {
                        $scope.msgNoExtrato = false;
                        $scope.extrato = res.data.extrato;
                    } else {
                        $scope.msgNoExtrato = true;
                    }
                } else
                    console.log('erro');
            },
            function(res) {
                console.log(res);
            }
        );
    }

    $scope.getIconExtratoType = function(type){
        if(type==1){
            return "-";
        }else{
            return "+";
        }
    }
});