angular.module('zampieri.solicitarpagamentos', [])


.controller('solicitarPagamentosCtrl', function($scope, $stateParams, api, $rootScope, popup, $state) {
    $scope.solicitacao = {}

    $scope.enviarSolicicataoPagamento = function() {

        if ($scope.solicitacao.valor_pagamento > $rootScope.dataLogin.usuario.saldo) {
            popup("Opsss", "Você não possui esse saldo");
            return;
        } else {
            $scope.solicitacao.usuario_id = $rootScope.dataLogin.usuario.id;

            api.enviarSolicitacaoPagamento($scope.solicitacao,
                function(res) {
                    if (res.data.status == 200) {
                        $rootScope.dataLogin.usuario.saldo = $rootScope.dataLogin.usuario.saldo - $scope.solicitacao.valor_pagamento;
                        $rootScope.atualizarSaldo();
                        popup("Sucesso", res.data.msg)
                        $state.go('tab.dash');
                        console.log(res.data);
                    } else
                        popup("Opss...", res.data.msg);
                },
                function(res) {
                    console.log(res);
                }
            );
        }
    }

});