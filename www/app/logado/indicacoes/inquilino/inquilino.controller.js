angular.module('zampieri.indicarinquilino', [])

.controller('indicarInquilinoCtrl', function($scope, $stateParams, api, $rootScope, popup, $state) {
    $scope.indicacaoData = {};
    $scope.enviarIndicacaoInquilino = function() {
        // criar validações
        $scope.indicacaoData.usuario_id = $rootScope.dataLogin.usuario.id;
        api.enviarIndicacaoInquilino($scope.indicacaoData,
            function(res) {
                if (res.data.status == 200) {
                    console.log(res.data);
                    $scope.indicacaoData = {};
                    popup("Indicação enviada com sucesso!, Nossa equipe irá validar os dados e retornará o status da sua indicação em breve", res.data.msg);
                    $state.go('tab.indicar');

                } else {
                    console.log('erro');
                    popup("Opss..., Parece que algo deu errado. Tente novamente", res.data.msg);
                }
            },
            function(res) {
                console.log(res);
            }
        );
    }


});