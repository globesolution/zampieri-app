angular.module('zampieri.changepassword', [])


.controller('changepasswordCtrl', function($scope, $state, $rootScope, api, popup) {

    $scope.passwordData = {};
    $scope.newPassword = function(){

        $scope.passwordData.usuario_id = $rootScope.dataLogin.usuario.id;
        api.changePassword($scope.passwordData,
            function(res) {
                if (res.data.status == 200) {
                    console.log(res.data);
                    popup("Sucesso", res.data.msg);

                } else {
                    console.log('erro');
                    popup("Opss...", res.data.msg);
                }
            },
            function(res) {
                console.log(res);
            }
        );
    } 
});