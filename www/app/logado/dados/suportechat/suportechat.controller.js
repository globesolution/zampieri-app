angular.module('zampieri.suportechat', [])

.controller('suportechatCtrl', function($scope, $rootScope, $state, api, popup, $location, $anchorScroll, $controller) {
   
    $scope.chat = {};
    $scope.chatmsgs = [];
 

    $scope.getNome = function(type){
        if(type == 1)
            return 'Eu';
        else
            return 'Suporte';
    }
    $scope.enviarMsg = function(){
        data = {
            usuario_id: $rootScope.dataLogin.usuario.id,
            msg: $scope.chat.msg
        };
        api.enviarMsg(data,
            function(res) {
                if (res.data.status == 200) {
                    console.log(res.data);
                    $scope.chatmsgs.push({msg:$scope.chat.msg, tipo_user:1});
                    $scope.chat.msg = '';

                } else {
                    console.log('erro');
                    popup("Opss...", res.data.msg);
                }
            },
            function(res) {
                console.log(res);
            }
        );
    }


    $scope.getMsgs = function(){

        $scope.chatmsgs = [];
        data = {
            usuario_id: $rootScope.dataLogin.usuario.id
        };
        api.getMsgs(data,
            function(res) {
                if (res.data.status == 200) {
                    $scope.chatmsgs = res.data.msgs;
                    console.log($scope.chatmsgs);

                } else {
                    console.log('erro');
                    popup("Opss...", res.data.msg);
                }
            },
            function(res) {
                console.log(res);
            }
        );
    }
})
