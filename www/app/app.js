// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('zampieri', [
    'ionic',
    'ngCordova',
    'ngMask',
    'zampieri.cadastro',
    'zampieri.login',
    'zampieri.remember',
    'zampieri.changepassword', 
    'zampieri.suportechat',
    'zampieri.account',
    'zampieri.extrato',
    'zampieri.indicacoesrealizadas',
    'zampieri.solicitarpagamentos',
    'zampieri.dashboard',
    'zampieri.indicarimovel',
    'zampieri.indicarinquilino', 
    'zampieri.tab', 
    'zampieri.services',
    'ionic-material',
    'ngScrollGlue'
    ])

.run(function($ionicPlatform, $cordovaSQLite, $rootScope, $cordovaSplashscreen) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        if (window.cordova) {
            db = window.sqlitePlugin.openDatabase({name: 'zampieri.db', location: 'default'});
        } else {
            db = window.openDatabase("zampieri.db", '1', 'zampieri', 1024 * 1024 * 100); // browser
        }

        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS user (id integer primary key, email text, password text)");
        $rootScope.checkDB();
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $ionicConfigProvider.backButton.text('').icon('ion-chevron-left');
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'app/deslogado/login/login.html',
            controller: 'loginCtrl'
        })
        .state('cadastrar', {
            url: '/cadastrar',
            templateUrl: 'app/deslogado/cadastrar/cadastrar.html',
            controller: 'cadastrarCtrl'
        })
        .state('remember', {
            url: '/remember',
            templateUrl: 'app/deslogado/remember/remember.html',
            controller: 'rememberCtrl'
        })

    // setup an abstract state for the tabs directive
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'app/logado/tabs.html',
        onEnter: function($state, $rootScope, popup){
            if(typeof($rootScope.dataLogin) == "undefined"){
                popup("Opsss", "Parece que você não está logado");
                $state.go('login');
            }
        }
    })

    // Each tab has its own nav history stack:

    .state('tab.dash', {
        url: '/dash',
        views: {
            'tab-dash': {
                templateUrl: 'app/logado/dashboard/tab-dash.html',
                controller: 'DashCtrl'
            }
        }
    })
    .state('tab.extrato', {
            url: '/extrato',
            views: {
                'tab-dash': {
                    templateUrl: 'app/logado/dashboard/extrato/extrato.html',
                    controller: 'extratoCtrl'
                }
            }
        })
    .state('tab.solicitarpagamentos', {
            url: '/solicitarpagamentos',
            views: {
                'tab-dash': {
                    templateUrl: 'app/logado/dashboard/solicitarpagamentos/solicitarpagamentos.html',
                    controller: 'solicitarPagamentosCtrl'
                }
            }
        })


    //end dashboard
    //start indications
    .state('tab.indicacoesrealizadas', {
            url: '/indicacoesrealizadas',
            views: {
                'tab-dash': {
                    templateUrl: 'app/logado/dashboard/indicacoesrealizadas/indicacoesrealizadas.html',
                    controller: 'indicacoesRealizadasCtrl'
                }
            }
        })

    .state('tab.indicar', {
            url: '/indicar',
            views: {
                'tab-indicar': {
                    templateUrl: 'app/logado/indicacoes/tab-indicar-imovel.html'
                }
            }
        })
        .state('tab.indicar-imovel', {
            url: '/imovel',
            views: {
                'tab-indicar': {
                    templateUrl: 'app/logado/indicacoes/imovel/form-imovel.html',
                    controller: 'indicarImovelCtrl'
                }
            }
        })
        .state('tab.indicar-inquilino', {
            url: '/inquilino',
            views: {
                'tab-indicar': {
                    templateUrl: 'app/logado/indicacoes/inquilino/form-inquilino.html',
                    controller: 'indicarInquilinoCtrl'
                }
            }
        })
        
    //end indication
    //start account
    .state('tab.account', {
        url: '/account',
        views: {
            'tab-account': {
                templateUrl: 'app/logado/dados/tab-account.html',
                controller: 'AccountCtrl'
            }
        }
    })
    .state('tab.changepassword', {
        url: '/changepassword',
        views: {
            'tab-account': {
                templateUrl: 'app/logado/dados/changepassword/changepassword.html',
                controller: 'changepasswordCtrl'
            }
        }
    })
    .state('tab.suportechat', {
        url: '/suportechat',
        views: {
            'tab-account': {
                templateUrl: 'app/logado/dados/suportechat/suportechat.html',
                controller: 'suportechatCtrl'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');

});