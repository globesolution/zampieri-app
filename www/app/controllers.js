angular.module('zampieri.tab', [])

//Controller popover

.controller('tab', function($scope, $ionicPopover, $rootScope) {
  $scope.noti=false;
  $scope.logado = true;
  $scope.msgNoPopNot = false;

  $ionicPopover.fromTemplateUrl('app/logado/popover-notes.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $rootScope.changeButtonStatus = function(){
    console.log($rootScope.dataLogin);
    if(typeof($rootScope.dataLogin) == "undefined"){
      $scope.logado = true;
    }else{
      $scope.logado = false;
    }
    console.log($scope.logado)
  }
  $scope.openPopover = function($event) {
    $scope.popover.show($event);
    $scope.noti=true;
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };

  $scope.msgNoti = function(tipo_msg){
      if(tipo_msg == 1){
        return 'foi depositado em sua conta bancária';
      }else{
        return 'foi creditado em sua conta Zampieri';
      }
  }
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });
});